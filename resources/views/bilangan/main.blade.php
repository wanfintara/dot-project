<!DOCTYPE html>
<html>
   <head>
      <meta charset="utf-8">
      <title>Bilangan Terbesar II</title>
      <link rel="stylesheet" href="{{ asset('css/bootstrap.min.css') }}">
      <link rel="stylesheet" href="{{ asset('css/bootstrap-theme.min.css') }}">
   </head>
   <body>
      <div class="container">
         <div class="jumbotron">
            <h1 class="text-center">Cari Bilangan Terbesar II</h1>
            <div class="row">
               <div class="col-md-12">
                  <div class="col-md-8">
                     <div class="form-group">
                        <label>Masukkan Bilangan sebanyak mungkin (Seperator SPASI):</label>
                        <input id="input" type="text" class="form-control">
                     </div>
                  </div>
                  <div class="col-md-4">
                     <button class="btn btn-success" type="button" name="button" onclick="cari()">Cari</button>
                  </div>
               </div>
               <div class="col-md-12">
                  <div class="col-md-3">
                     <h5>Terbesar II : </h5>
                  </div>
                  <div class="col-md-3">
                     <strong><h4 id="hasil"></h4></strong>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </body>
</html>

<script src="{{ asset('js/jquery-3.3.1.min.js') }}"></script>
<script src="{{ asset('js/bootstrap.min.js') }}"></script>
<script type="text/javascript">
   $(function functionName() {


   });

   function cari() {
      var str = $("#input").val();
      var temp = new Array();
      temp = str.split(" ");
      // console.log(temp);
      for (i = 0; i < temp.length; i++) {
         temp[i] = parseInt(temp[i]);
      }
      var top1 = top2 = temp[0];

      for (i = 1; i < temp.length; i++) {
         if (temp[i] > top1){
            top2 = top1;
            top1 = temp[i];
         }
         else if (temp[i] > top2 && temp[i] != top1) {
            top2 = temp[i];
         }
      }
      // console.log(top1);
      // console.log(top2);
      $("#hasil").text(top2);
   }
</script>
