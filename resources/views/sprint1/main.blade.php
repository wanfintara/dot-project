<!DOCTYPE html>
<html>
   <head>
      <meta charset="utf-8">
      <title>Sprint 1</title>
      <link rel="stylesheet" href="{{ asset('css/bootstrap.min.css') }}">
      <link rel="stylesheet" href="{{ asset('css/bootstrap-theme.min.css') }}">
   </head>
   <body>
      <div class="container">
         <div class="jumbotron">
            <h1 class="text-center">Sprint 1</h1>
            <div class="row">
               <div class="col-md-12">

                  <div class="row">
                     <div class="col-md-6">
                        <a href="" class="btn btn-primary btn-block">AMBIL DATA API</a>
                     </div>
                  </div>

                  <div class="row">
                     <div class="col-md-6">
                        <div class="form-group">
                           <label>Masukkan ID Provinsi:</label>
                           <input id="input_provinsi" type="text" class="form-control">
                        </div>
                     </div>
                     <div class="col-md-6">
                        <h5 id="text_provinsi"></h5>
                     </div>
                  </div>

                  <div class="row">
                     <div class="col-md-6">
                        <div class="form-group">
                           <label>Masukkan ID Kota:</label>
                           <input id="input_kota" type="text" class="form-control">
                        </div>
                     </div>
                     <div class="col-md-6">
                        <h5 id="text_kota"></h5>
                     </div>
                  </div>

                  <div class="row">
                     <div class="col-md-6">
                        <div class="form-group">
                           <label>Masukkan ID (API Raja Ongkir) Testing:</label>
                           <input id="input" type="text" class="form-control">
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </body>
</html>

<script src="{{ asset('js/jquery-3.3.1.min.js') }}"></script>
<script src="{{ asset('js/bootstrap.min.js') }}"></script>
<script type="text/javascript">
   $(function() {

      $("#input_provinsi").keyup(function () {
            var id = $("#input_provinsi").val();
            // var route = "{{ route('api.province') }}";
            var route = "{{ config('constant.api_province') }}";
            // console.log(id);
            if (id != "") {
               $.ajax({
                  type    : 'get',
                  url     : route,
                  data    : {'id' : id},
                  success : function(data) {
                     console.log(data);
                     $("#text_provinsi").text(data);
                  }
               });
            }
      });

      $("#input_kota").keyup(function () {
            var id = $("#input_kota").val();
            // var route = "{{ route('api.city') }}";
            var route = "{{ config('constant.api_city') }}";
            if (id != "") {
               $.ajax({
                  type    : 'get',
                  url     : route,
                  data    : {'id' : id},
                  success : function(data) {
                     // console.log(data);
                     $("#text_kota").text(data);
                  }
               });
            }
      });

      //NOTE Coba lgsg ke API rajaongkir tapi gagal, dunno why XD, pada browser sudah support CORS
      $("#input").keyup(function () {
            var id = $("#input").val();
            var route = "https://api.rajaongkir.com/starter/province";
            // console.log(id);
            if (id != "") {

               $.ajax({
                  url     : route,
                  type    : 'get',
                  headers : { 'key': '0df6d5bf733214af6c6644eb8717c92c'}
               });
            }
      });
   });
</script>
