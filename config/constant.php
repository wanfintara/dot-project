<?php

return [
   'api_province' => env('API_PROVINCE','http://localhost/dot_test/public/search/provinces'),
   'api_city' => env('API_CITY','http://localhost/dot_test/public/search/cities'),
];
