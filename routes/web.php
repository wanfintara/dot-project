<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
})->name('welcome');

Route::get('/sprint1', 'Sprint1Controller@main')->name('sprint');

Route::get('/bilangan', 'BilanganController@main')->name('bilangan');

Route::get('/search/provinces', 'ApiController@province')->name('api.province');
Route::get('/search/cities', 'ApiController@city')->name('api.city');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
