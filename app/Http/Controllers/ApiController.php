<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;

class ApiController extends Controller
{
   public function main() {
      return view('bilangan.main');
   }

   public function province(Request $r) {
      if ($r->ajax()){
         $provinsi = DB::table('provinces')->where('id', '=', $r->id)->first();
         // dd($provinsi);
         if ($provinsi) {
            // dd("yes");
            return $provinsi->province;
         }
         else {
            // dd("No");
            return "Tidak ada provinsi";
         }
      }
   }

   public function city(Request $r) {
      if ($r->ajax()){
         $kota = DB::table('cities')->where('id', '=', $r->id)->first();
         if ($kota) {
            return $kota->city_name;
         }
         else {
            return "Tidak ada kota";
         }
      }
   }
}
