<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use GuzzleHttp\Client;
use DB;
use Carbon\Carbon;

class Sprint1Controller extends Controller
{
   public function __construct() {
      $this->middleware('auth');
   }

   public function main() {
      return view('sprint1.main');
   }

   public function AmbilData() {
      $client = new Client();
      $res1 = $client->request('GET', 'https://api.rajaongkir.com/starter/province', [
         'headers' => [
            'key' => '0df6d5bf733214af6c6644eb8717c92c'
         ]
      ]);

      $res2 = $client->request('GET', 'https://api.rajaongkir.com/starter/city', [
         'headers' => [
            'key' => '0df6d5bf733214af6c6644eb8717c92c'
         ]
      ]);

      $provinsi = $res1->getBody();
      $provinsi = json_decode($provinsi);

      $kota = $res2->getBody();
      $kota = json_decode($kota);
      // $x = $provinsi->rajaongkir->results;
      // dd($provinsi->rajaongkir->results);
      $data_provinsi = array();
      $data_kota = array();

      $created_at = Carbon::now();
      //NOTE karena array decode di JSON itu jenis array object
      foreach ($provinsi->rajaongkir->results as $key => $value) {
         $temp['id'] =  $value->province_id;
         $temp['province'] = $value->province;
         $temp['created_at'] = $created_at;
         $data_provinsi[] = $temp;
      }
      foreach ($kota->rajaongkir->results as $key => $value) {
         $temp['id'] =  $value->city_id;
         $temp['province_id'] = $value->province_id;
         $temp['province'] = $value->province;
         $temp['type'] = $value->type;
         $temp['city_name'] = $value->city_name;
         $temp['postal_code'] = $value->postal_code;
         $temp['created_at'] = $created_at;
         $data_kota[] = $temp;
      }

      DB::table('provinces')->insert($data_provinsi);
      DB::table('cities')->insert($data_kota);
      dd("Data berhasil masuk");
   }
}
